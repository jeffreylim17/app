package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Link {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String self;
}
