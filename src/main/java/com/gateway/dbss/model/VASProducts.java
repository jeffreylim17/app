package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VASProducts {

    private String paymentType;
    private String status;
    private String relationshipCode;
    private String includedCode;
    private String connectedProducts;
    private String connectedProductsId;
    private String connectedProductsType;
    private String mediumDescription;

    private List<String> codeList;

    public VASProducts codeList(List<String> codeList) {
        this.codeList = codeList;
        return this;
    }

    public VASProducts addCode(String code) {
        if (codeList.isEmpty()) {
            this.codeList = new ArrayList<>();
        }
        this.codeList.add(code);
        return this;
    }
}
