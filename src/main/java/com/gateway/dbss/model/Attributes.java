package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Attributes {
    //    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("activation-time")
//    private String activationTime;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("bv-status")
//    private String bvStatus;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("contract-id")
//    private String contractId;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("contract-status")
//    private String contractStatus;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("directory-listing")
//    private String directoryListing;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("first-call-date")
//    private String firstCallDate;
//
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("is-biometric-verified")
//    private boolean isBiometricVerified;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("is-mfs")
//    private boolean isMfs;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("latest-contract-termination-time")
//    private String latestContractTerminationTime;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("management-msisdn")
//    private String managementMsisdn;
//
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("monthly-costs")
//    private int monthlyCosts;
//
//
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("msisdn")
    private String msisdn;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("original-contract-confirmation-code")
//    private String originalContractConfirmationCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("payment-type")
    private String paymentType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("status")
    private String status;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("termination-time")
//    private String terminationTime;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String code;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("medium-description")
    private Dictionary mediumDescription;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("first-name")
    private String firstName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("gender")
    private String gender;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id-document-number")
    private String idDocumentNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("address-type")
    private String addressType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String aparment;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String building;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String city;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String country;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String brand;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("business-model-type")
    private String businessModelType;


}
