package com.gateway.dbss.model.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductData {

    @JsonProperty("type")
    private String type;
    @JsonProperty("id")
    private String id;
    @JsonProperty("attributes")
    private ProductAttributes attributes;

    @JsonProperty("detail")
    private String detail;
    @JsonProperty("source")
    private ProductSource source;
    @JsonProperty("status")
    private String status;
    @JsonProperty("code")
    private String code;
}

