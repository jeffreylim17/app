package com.gateway.dbss.model.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductErrorResponse {

    private String detail;
    private ProductSource source;
    private String status;
    private String code;

}
