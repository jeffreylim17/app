package com.gateway.dbss.model.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductAttributes {

    @JsonProperty("request-id")
    private String requestId;
    private String href;
    @JsonProperty("scheduled-at")
    private String scheduledAt;
    private String status;
}
