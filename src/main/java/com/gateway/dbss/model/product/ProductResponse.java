package com.gateway.dbss.model.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductResponse {
    private String status;
    private String detail;
    @JsonProperty("scheduled-at")
    private String scheduledAt;

    public ProductResponse() {

    }

    public ProductResponse(String status, String detail, String scheduledAt) {
        this.status = status;
        this.detail = detail;
        this.scheduledAt = scheduledAt;
        return;
    }
}
