package com.gateway.dbss.model.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductRequest {
    private String type;
    private String id;
    private ProductMeta meta;
}
