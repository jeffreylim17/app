package com.gateway.dbss.model.product;

import lombok.Data;

@Data
public class ErrorResponse {
    private String errors;
}
