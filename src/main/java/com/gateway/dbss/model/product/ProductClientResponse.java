package com.gateway.dbss.model.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProductClientResponse {
    @JsonProperty("errors")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ErrorResponse> errors;

    @JsonProperty("data")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ProductData> data;
}