package com.gateway.dbss.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProductDeactivationRequest {
    @JsonProperty("data")
    private List<ProductRequest> data;
}
