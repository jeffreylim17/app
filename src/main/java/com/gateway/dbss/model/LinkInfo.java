package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LinkInfo {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private RelatedLinks links;

    private List<Tags> dataList;

    private Tags data;

    public LinkInfo links(RelatedLinks relatedLinks) {
        this.links = relatedLinks;
        return this;
    }
}
