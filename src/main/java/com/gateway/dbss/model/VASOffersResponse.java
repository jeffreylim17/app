package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VASOffersResponse {
    private String msisdn;
    private String paymentType;
    private String status;
    @JsonProperty("available-products")
    private Products availableProducts;
    private List<String> codeList;

    public VASOffersResponse codeList(List<String> codeList) {
        this.codeList = codeList;
        return this;
    }

    public VASOffersResponse addCode(String code) {
        if (codeList.isEmpty()) {
            this.codeList = new ArrayList<>();
        }
        this.codeList.add(code);
        return this;
    }
}
