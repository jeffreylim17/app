package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Relationships {

    //    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("available-child-products")
//    private LinkInfo availableChildProducts;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("available-loan-products")
//    private LinkInfo availableLoanProducts;
//
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("available-products")
    private Products availableProducts;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("available-subscription-types")
//    private LinkInfo availableSubscriptionTypes;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("balances")
//    private LinkInfo balances;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("barrings")
//    private LinkInfo barrings;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("billing-accounts")
//    private LinkInfo billingAccounts;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("billing-rate-plan")
//    private LinkInfo billingRatePlan;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("billing-usages")
//    private LinkInfo billingUsages;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("catalog-sim-cards")
//    private LinkInfo catalogSimCards;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("combined-usage-reports")
//    private LinkInfo combinedUsageReports;

    @JsonProperty("connected-products")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Products connectedProducts;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("connection-type")
//    private LinkInfo connectionType;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("coordinator-customer")
//    private LinkInfo coordinatorCustomer;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("document-validations")
//    private LinkInfo documentValidations;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("gsm-service-usages")
//    private LinkInfo gsmServiceUsages;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("network-services")
//    private LinkInfo networkServices;
//
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("owner-customer")
//    private LinkInfo ownerCustomer;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("payer-customer")
//    private LinkInfo payerCustomer;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("porting-requests")
//    private LinkInfo portingRequests;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("product-usages")
//    private LinkInfo productUsages;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("products")
//    private LinkInfo products;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("services")
//    private LinkInfo services;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("sim-card-orders")
//    private LinkInfo simCardOrders;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("sim-cards")
//    private LinkInfo simCards;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("subscription-category")
//    private LinkInfo subscriptionCategory;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("subscription-discounts")
//    private LinkInfo subscriptionDiscounts;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("subscription-type")
//    private LinkInfo subscriptionType;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("user-customer")
//    private LinkInfo userCustomer;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("activation-template-id")
//    private List<String> activationTemplateId;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("allow-re-activation")
//    private boolean allowReActivation;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("charge-type")
//    private String chargeType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("code")
    private String code;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("deactivation-template-id")
//    private List<String> deactivationTemplateId;
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @JsonProperty("display-order")
//    private int displayOrder;

}