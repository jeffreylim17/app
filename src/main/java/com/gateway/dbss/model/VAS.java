package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@lombok.Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VAS {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Data> data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Data> included;

}
