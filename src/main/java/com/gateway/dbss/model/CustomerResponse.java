package com.gateway.dbss.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerResponse {
    private String msisdn;
    private String paymentType;
    private String status;
    private String email;
    private String firstName;
    private String gender;
    private String idDocumentNumber;
    private String addressType;
    private String apartment;
    private String building;
    private String city;
    private String country;
    private String brand;
    private String businessModelType;

}