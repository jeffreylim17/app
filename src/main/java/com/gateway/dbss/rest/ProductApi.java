package com.gateway.dbss.rest;

import com.gateway.dbss.model.product.ProductActivationRequest;
import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "${feign.name}", url = "${feign.basePath}")
public interface ProductApi {

    @PostMapping(value = "${feign.activate}", consumes = "application/vnd.api+json")
    Response activate(@RequestBody ProductActivationRequest productActivationRequest, @PathVariable("msisdn") String msisdn);

    @DeleteMapping(value = "${feign.deactivate}", consumes = "application/vnd.api+json")
    Response deactivate(@RequestBody ProductActivationRequest productActivationRequest, @PathVariable("msisdn") String msisdn);

}
