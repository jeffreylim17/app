package com.gateway.dbss.controller;


import com.gateway.dbss.model.CustomerResponse;
import com.gateway.dbss.model.VASOffersResponse;
import com.gateway.dbss.model.VASProducts;
import com.gateway.dbss.model.product.ProductResponse;
import com.gateway.dbss.service.MapperService;
import com.gateway.dbss.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MapperController {

  @Autowired
  private MapperService mapperService;

  @Autowired
  private ProductService productService;

  @GetMapping(value = "/vas/products", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<VASProducts> checkSubscribedVASProducts(
      @RequestParam(name = "msisdn") String msisdn,
      @RequestParam(name = "referenceId") String referenceId) {
    return mapperService.checkSubscribedVASProducts(msisdn, referenceId);
  }

  @GetMapping(value = "/vas/offers", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<VASOffersResponse> checkEligibleVASOffers(
      @RequestParam(name = "msisdn") String msisdn,
      @RequestParam(name = "referenceId") String referenceId) {
    return mapperService.checkEligibleVASOffers(msisdn, referenceId);
  }

  @GetMapping(value = "/customer/", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CustomerResponse> getCustomerDetails(
      @RequestParam(name = "msisdn") String msisdn,
      @RequestParam(name = "referenceId") String referenceId) {
    return mapperService.getCustomerDetails(msisdn, referenceId);
  }

  @GetMapping(value = "/product/activate", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ProductResponse> productActivation(
      @RequestParam(name = "msisdn") String msisdn,
      @RequestParam(name = "referenceId") String referenceId,
      @RequestParam(name = "subscriptionType") String subscriptionType) {
    return productService.activate(msisdn, referenceId, subscriptionType);
  }

  @GetMapping(value = "/product/deactivate", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ProductResponse> productDeActivation(
      @RequestParam(name = "msisdn") String msisdn,
      @RequestParam(name = "referenceId") String referenceId) {
    return productService.deactivate(msisdn, referenceId);
  }
}