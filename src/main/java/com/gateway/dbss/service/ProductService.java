package com.gateway.dbss.service;

import com.gateway.dbss.model.product.ProductResponse;
import org.springframework.http.ResponseEntity;

public interface ProductService {

    ResponseEntity<ProductResponse> activate(String msisdn, String referenceId, String subscriptionType);
    ResponseEntity<ProductResponse> deactivate(String msisdn, String referenceId);

}
