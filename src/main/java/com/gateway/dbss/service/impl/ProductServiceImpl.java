package com.gateway.dbss.service.impl;

import com.gateway.dbss.model.product.*;
import com.gateway.dbss.rest.ProductApi;
import com.gateway.dbss.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductApi productApi;

  @Autowired
  private RestTemplate restTemplate;

  @Value("${client.url.base}")
  private String baseUrl;


  @Override
  public ResponseEntity<ProductResponse> activate(String msisdn, String referenceId,
      String subscriptionType) {
    log.info("START :: ACTIVATE :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("Accept", MediaType.ALL_VALUE);
    headers.add("Content-type", "application/vnd.api+json");
    ProductActivationRequest productActivationRequest = initializeActivateProductRequest(
        referenceId, subscriptionType, msisdn);

    HttpEntity entity = new HttpEntity(productActivationRequest, headers);
    String url =
        baseUrl + "api/v1/subscriptions/" + msisdn + "/relationships/products?identifier=msisdn";
    log.info(" Before API Request :: [{}]", referenceId);
    ResponseEntity<ProductResponse> response = new ResponseEntity<>(
        new ProductResponse("failed", "Error on processing data", null), HttpStatus.BAD_REQUEST);
    ResponseEntity<ProductClientResponse> apiResponse = ResponseEntity.badRequest().build();
    try {
      apiResponse = restTemplate
          .exchange(url, HttpMethod.POST, entity, ProductClientResponse.class);
    } catch (HttpClientErrorException e) {
      log.info("API Request Exception [{}]  :: reference id :: [{}]", e.getLocalizedMessage(),
          referenceId);
    }
    log.info(" After API Request :: [{}]", referenceId);
    log.info(" Client API Response is [{}] body [{}] referenceId [{}]", apiResponse.getStatusCode(),
        apiResponse.getBody(), referenceId);
    if (apiResponse.getStatusCode().is2xxSuccessful()) {
      log.info("Building Payload For Success referenceId [{}]", referenceId);
//      ProductClientResponse resp = apiResponse.getBody();
      ProductResponse productResponse = new ProductResponse();
      productResponse.setStatus(apiResponse.getBody().getData().get(0).getAttributes().getStatus());
      productResponse.setScheduledAt(apiResponse.getBody().getData().get(0).getAttributes().getScheduledAt());
      response = ResponseEntity.ok(productResponse);
    }
    log.info("END :: ACTIVATE :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
    return response;
  }

  @Override
  public ResponseEntity<ProductResponse> deactivate(String msisdn, String referenceId) {
    log.info("START :: DEACTIVATE :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("Accept", MediaType.ALL_VALUE);
    headers.add("Content-type", "application/vnd.api+json");
    ProductDeactivationRequest productDeactivationRequest = initializeDeActivateProductRequest(
        referenceId);
    HttpEntity entity = new HttpEntity(productDeactivationRequest, headers);
    String url =
        baseUrl + "api/v1/subscriptions/" + msisdn + "/relationships/products?identifier=msisdn";
    log.info(" Before API Request :: [{}]", referenceId);
    ResponseEntity<ProductResponse> response = new ResponseEntity<>(
        new ProductResponse("failed", "Error on processing data", null), HttpStatus.BAD_REQUEST);
    ResponseEntity<ProductClientResponse> apiResponse = ResponseEntity.badRequest().build();

    try {
      apiResponse = restTemplate
          .exchange(url, HttpMethod.DELETE, entity, ProductClientResponse.class);
    } catch (HttpClientErrorException e) {
      log.info("API Request Exception [{}]  :: reference id :: [{}]", e.getLocalizedMessage(),
          referenceId);
    }

    log.info(" After API Request :: [{}]", referenceId);
    log.info(" Client API Response is [{}] body [{}] referenceId [{}]", apiResponse.getStatusCode(),
        apiResponse.getBody(), referenceId);
    if (apiResponse.getStatusCode().is2xxSuccessful()) {
      log.info("Building Payload For Success referenceId [{}]", referenceId);
//      ProductClientResponse resp = apiResponse.getBody();
      ProductResponse productResponse = new ProductResponse();
      productResponse.setStatus(apiResponse.getBody().getData().get(0).getAttributes().getStatus());
      productResponse.setScheduledAt(apiResponse.getBody().getData().get(0).getAttributes().getScheduledAt());
      response = ResponseEntity.ok(productResponse);
    }

    log.info("END :: DEACTIVATE :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
    return response;
  }

  private ProductActivationRequest initializeActivateProductRequest(String referenceId,
      String subscriptionType, String msisdn) {
    ProductActivationRequest productActivationRequest = new ProductActivationRequest();
    ProductRequest productRequest = new ProductRequest();
    productRequest.setType("products");
//        productRequest.setId("CALLSMSBLCKRECUR");

    if (subscriptionType.equals("prepaid")) {
      productRequest.setId("AUTOCALLBACKPREDAILY");
    } else {
      productRequest.setId("AUTOCALLBACKMONTHPOST");
    }

    ProductMeta meta = new ProductMeta();
    meta.setChannel("BSSAPI");
    productRequest.setMeta(meta);

    productActivationRequest.setData(productRequest);
    log.info("product activation request [{}] referenceId [{}] :: msisdn [{}]",
        productActivationRequest, referenceId, msisdn);
    return productActivationRequest;
  }

  private ProductDeactivationRequest initializeDeActivateProductRequest(String referenceId) {
    ProductDeactivationRequest productDeactivationRequest = new ProductDeactivationRequest();
    ProductRequest productRequest = new ProductRequest();
    productRequest.setType("products");
    productRequest.setId("AUTOCALLBACKPREDAILY");
    ProductMeta meta = new ProductMeta();
    meta.setChannel("BSSAPI");
    productRequest.setMeta(meta);

    productDeactivationRequest.setData(Arrays.asList(productRequest));
    log.info("product deactivation request [{}] referenceId [{}]", productDeactivationRequest,
        referenceId);
    return productDeactivationRequest;
  }
}
