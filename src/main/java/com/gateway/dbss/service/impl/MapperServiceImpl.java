package com.gateway.dbss.service.impl;

import com.gateway.dbss.model.CustomerResponse;
import com.gateway.dbss.model.VAS;
import com.gateway.dbss.model.VASOffersResponse;
import com.gateway.dbss.model.VASProducts;
import com.gateway.dbss.service.MapperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class MapperServiceImpl implements MapperService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${client.url.base}")
    private String baseUrl;

    public ResponseEntity<VASProducts> checkSubscribedVASProducts(String msisdn, String referenceId) {
        log.info("START :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.ALL_VALUE);
        HttpEntity entity = new HttpEntity(headers);

        String url = baseUrl + "api/v1/subscriptions?filter[msisdn]=" + msisdn + "&include=connected-products&filter[product-family-name]=vas";
        ResponseEntity<VAS> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, VAS.class);
        VAS vas = responseEntity.getBody();
        log.info(" Object mapped :: [{}] referenceId [{}]", responseEntity.getBody().toString(), referenceId);

        VASProducts vASProducts = new VASProducts();
        if (!ObjectUtils.isEmpty(vas.getData())) {
            log.info("Get payment Type :: referenceId [{}]", referenceId);
            if (StringUtils.hasText(vas.getData().get(0).getAttributes().getPaymentType()))
                vASProducts.setPaymentType(vas.getData().get(0).getAttributes().getPaymentType());


            log.info("Get Included Code :: referenceId [{}]", referenceId);
            if (StringUtils.hasText(vas.getIncluded().get(0).getAttributes().getCode()))
                vASProducts.setIncludedCode(vas.getIncluded().get(0).getAttributes().getCode());

            log.info("Get Status :: referenceId [{}]", referenceId);
            if (StringUtils.hasText(vas.getData().get(0).getAttributes().getStatus()))
                vASProducts.setStatus(vas.getData().get(0).getAttributes().getStatus());

            log.info("Get Connected Products :: referenceId [{}]", referenceId);
            if (!ObjectUtils.isEmpty(vas.getData().get(0).getRelationships().getConnectedProducts())) {
                vASProducts.setConnectedProducts(vas.getData().get(0).getRelationships().getConnectedProducts().getLinks().getRelated());
                vASProducts.setConnectedProductsId(vas.getData().get(0).getRelationships().getConnectedProducts().getData().get(0).getId());
                vASProducts.setConnectedProductsType(vas.getData().get(0).getRelationships().getConnectedProducts().getData().get(0).getType());

                log.info("Get Relationship Code :: referenceId [{}]", referenceId);
                if (StringUtils.hasText(vas.getData().get(0).getRelationships().getCode()))
                    vASProducts.setRelationshipCode(vas.getData().get(0).getRelationships().getConnectedProducts().getData().get(0).getId());

            }
            log.info("Get Medium Description Code :: referenceId [{}]", referenceId);
            if (!ObjectUtils.isEmpty(vas.getIncluded().get(0).getAttributes().getMediumDescription()))
                vASProducts.setMediumDescription(vas.getIncluded().get(0).getAttributes().getMediumDescription().getEn());
        }

        if (!ObjectUtils.isEmpty(vas.getIncluded())) {
            log.info("Get Codes :: referenceId [{}]", referenceId);
            List<String> codeList = new ArrayList<>();
            vas.getIncluded().stream().forEach(attribute -> {
                log.info("Included -> Attribute -> id [{}] referenceId [{}]", attribute.getId(), referenceId);
                if (StringUtils.hasText(attribute.getAttributes().getCode())) {
                    codeList.add(attribute.getAttributes().getCode());
                }
            });
            vASProducts.setCodeList(codeList);
        }

        log.info("END :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
        return ResponseEntity.ok(vASProducts);
    }

    @Override
    public ResponseEntity<VASOffersResponse> checkEligibleVASOffers(String msisdn, String referenceId) {
        log.info("START :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.ALL_VALUE);
        HttpEntity entity = new HttpEntity(headers);
        String url = baseUrl + "api/v1/subscriptions?filter[msisdn]=" + msisdn + "&include=available-products.fees";
        ResponseEntity<VAS> response = restTemplate.exchange(url, HttpMethod.GET, entity, VAS.class);
        VAS vas = response.getBody();
        log.info(" Object mapped :: [{}] :: referenceId [{}]", response.getBody().toString(), referenceId);

        VASOffersResponse vasOffersResponse = new VASOffersResponse();
        if (!ObjectUtils.isEmpty(vas.getData())) {
            log.info("Get msisdn :: referenceId [{}]", referenceId);
            vasOffersResponse.setMsisdn(vas.getData().get(0).getAttributes().getMsisdn());

            log.info("Get payment type :: referenceId [{}]", referenceId);
            vasOffersResponse.setPaymentType(vas.getData().get(0).getAttributes().getPaymentType());

            log.info("Get status :: referenceId [{}]", referenceId);
            vasOffersResponse.setStatus(vas.getData().get(0).getAttributes().getStatus());
            if (!ObjectUtils.isEmpty(vas.getData().get(0).getRelationships().getAvailableProducts().getData())) {
                log.info("Get available products :: referenceId [{}]", referenceId);
                vasOffersResponse.setAvailableProducts(vas.getData().get(0).getRelationships().getAvailableProducts());
            }

            if (!ObjectUtils.isEmpty(vas.getIncluded())) {
                log.info("Get Codes :: referenceId [{}]", referenceId);
                List<String> codeList = new ArrayList<>();
                vas.getIncluded().stream().forEach(attribute -> {
                    log.info("Included -> Attribute -> id [{}] referenceId [{}]", attribute.getId(), referenceId);
                    if (StringUtils.hasText(attribute.getAttributes().getCode())) {
                        codeList.add(attribute.getAttributes().getCode());
                    }
                });
                vasOffersResponse.setCodeList(codeList);
            }
        }
        log.info("END :: msisdn [{}] reference id :: [{}]", msisdn, referenceId);
        return ResponseEntity.ok(vasOffersResponse);
    }

    @Override
    public ResponseEntity<CustomerResponse> getCustomerDetails(String msisdn, String referenceId) {
        log.info("START :: Customer Details msisdn [{}] reference id :: [{}]", msisdn, referenceId);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.ALL_VALUE);
        HttpEntity entity = new HttpEntity(headers);
        String url = baseUrl + "api/v1/subscriptions?filter[msisdn]=" + msisdn + "&include=subscription-type,owner-customer.addresses";
        ResponseEntity<VAS> response = restTemplate.exchange(url, HttpMethod.GET, entity, VAS.class);
        VAS vas = response.getBody();
        log.info(" Object mapped :: [{}] :: referenceId [{}]", response.getBody().toString(), referenceId);

        CustomerResponse customerResponse = new CustomerResponse();

        if (!ObjectUtils.isEmpty(vas.getIncluded())) {

            log.info("Get msisdn :: referenceId [{}]", referenceId);
            customerResponse.setMsisdn(vas.getData().get(0).getAttributes().getMsisdn());

            log.info("Get payment type :: referenceId [{}]", referenceId);
            customerResponse.setPaymentType(vas.getData().get(0).getAttributes().getPaymentType());

            log.info("Get status :: referenceId [{}]", referenceId);
            customerResponse.setStatus(vas.getData().get(0).getAttributes().getStatus());

            log.info("Get email :: referenceId [{}]", referenceId);
            customerResponse.setEmail(vas.getIncluded().get(0).getAttributes().getEmail());

            log.info("Get First name :: referenceId [{}]", referenceId);
            customerResponse.setFirstName(vas.getIncluded().get(0).getAttributes().getFirstName());

            log.info("Get gender :: referenceId [{}]", referenceId);
            customerResponse.setGender(vas.getIncluded().get(0).getAttributes().getGender());

            log.info("Get id document number :: referenceId [{}]", referenceId);
            customerResponse.setIdDocumentNumber(vas.getIncluded().get(0).getAttributes().getIdDocumentNumber());

            log.info("Get address type :: referenceId [{}]", referenceId);
            customerResponse.setAddressType(vas.getIncluded().get(1).getAttributes().getAddressType());

            log.info("Get apartment :: referenceId [{}]", referenceId);
            customerResponse.setApartment(vas.getIncluded().get(1).getAttributes().getAparment());

            log.info("Get building :: referenceId [{}]", referenceId);
            customerResponse.setBuilding(vas.getIncluded().get(1).getAttributes().getBuilding());

            log.info("Get city :: referenceId [{}]", referenceId);
            customerResponse.setCity(vas.getIncluded().get(1).getAttributes().getCity());

            log.info("Get country :: referenceId [{}]", referenceId);
            customerResponse.setCountry(vas.getIncluded().get(1).getAttributes().getCountry());

            log.info("Get brand :: referenceId [{}]", referenceId);
            customerResponse.setBrand(vas.getIncluded().get(2).getAttributes().getBrand());

            log.info("Get business model type :: referenceId [{}]", referenceId);
            customerResponse.setBusinessModelType(vas.getData().get(0).getAttributes().getBusinessModelType());
        }

        log.info("END :: Customer Details msisdn [{}] reference id :: [{}]", msisdn, referenceId);
        return ResponseEntity.ok(customerResponse);
    }
}