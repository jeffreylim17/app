package com.gateway.dbss.service;

import com.gateway.dbss.model.CustomerResponse;
import com.gateway.dbss.model.VASOffersResponse;
import com.gateway.dbss.model.VASProducts;
import org.springframework.http.ResponseEntity;

public interface MapperService {
    ResponseEntity<VASProducts> checkSubscribedVASProducts(String msisdn, String referenceId);

    ResponseEntity<VASOffersResponse> checkEligibleVASOffers(String msisdn, String referenceId);

    ResponseEntity<CustomerResponse> getCustomerDetails(String msisdn, String referenceId);
}
